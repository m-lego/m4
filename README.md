5x12 in lego
===============

  ![M4 2x2](pics/m2x2-3d.png)

an ortholinear keyboard set in lego 2x22 with optional encoder and oled.

this is part of a bigger family of ortholinear keyboards in lego see for reference
https://alin.elena.space/blog/keeblego/

current status and more

https://gitlab.com/m-lego/m65

kicad symbols/footprints are in the m65 repo above

status: all ok

* [x] gerbers designed
* [x] firmware
* [x] breadboard tested
* [x] gerbers printed
* [ ] board tested

Features:

* 2x2
* 1 encoder, optional
* oled, optional
* 5 pins
* seeduino xiao nrf52840 or xiao rp2040, others too probably but not tested
* firmware zmk

the pcb
------

* kicad pcb

  ![M4 2x2 front pcb](pics/m4-front-pcb.png)

  ![M4 2x2 back pcb](pics/m4-back-pcb.png)

* 3d render

  ![M4 pcb](pics/m2x2-3d.png)


parts
----

* 1 xiao nrf52840 or rp2040  pins
* 4 signal diodes 1N4148 , do 35 or sod-123
* 1 encoders
* 2x7 pin DIL/DIP sockets whatever you prefer
* 2x7 pin male headers, rounded or straight to match the socket
* 5 pin MX switches 4
* lego 8x12 plate for bottom, and bricks as you please


repo
----

gerbers and kicad files in here  gitlab repo https://gitlab.com/m-lego/m4/

firmware
--------

to be made public once pcb tested

other pics
----------


